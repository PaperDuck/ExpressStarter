const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('../config');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.sign({ sub: user.id, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next) {
  // User has already had their email and password auth'd
  // We just need to give them a token
  res.send({ token: tokenForUser(req.user) });
}

exports.signup = function(req, res, next) {
  const email = req.body.email;
  const password = req.body.password;

  if(!email || !password) {
    return res.status(400).send({ error: "You must provide email and password" });
  }

  // Check to see if a user with the given email exist
  User.findOne({ email: email }, function(err, existingUser) {
    if(err) {
      return next(err);
    }

    // If a user exist
    if(existingUser) {
      return res.status(422).send({ error: "Email is in use" });
    }

    // If a user with the email does not exist, we will create one
    const user = new User({
      email: email,
      password: password
    });

    user.save(function(err) {
      if(err) {
        return next(err);
      }

      res.json({ token: tokenForUser(user) });
    });
  });
}