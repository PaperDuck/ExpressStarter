## Table of Contents

---

- [General](#general)
- [Environment Setup](#environment-setup)
- [Directory Breakdown](#directory-breakdown)
    - [Controllers](#controllers)
    - [Models](#models)
    - [Services](#services)
- [Configuration](#configuration)
    - [Server](#server)
    - [Routing](#routing)
    - [Secret](#secret)
- [Deployment Instructions](#deployment-instructions)

### General
This is a boilerplate project for Express. It comes configured with the following:
* Authentication using JWT. Authentication Strategies are managed by the **passport** package. So you can easily switch out the JWT for other alternatives. 
* There is a user model that is configured to work with MongoDB using the ODM **Mongoose**. 
* Passwords are hashed using the **bcryptjs** package.
* Hot reloading of server using **Nodemon**.
* Log server details using **Morgan**.
* Parse only incoming json-type requests using **body-parser**.

Feel free to use it! :)

### Environment Setup
1. Install NodeJS and NPM. You can grab the installer (which installs both) from https://nodejs.org/en/
2. On the Terminal, change to the project directory and key in the following command.

    > npm install

### Directory Breakdown

#### Controllers
This is where the routes are created. Let's say you need a route to access certain user data. You can create a *users.js* file to manage all the routes for it. (Think REST).

#### Models
This is where we define the data structure for our database records. So, to create a blueprint for our user. We create a *user.js* file for it and define the intricacies using Mongoose, a Javascript ODM for MongoDB.

#### Services
This is where the business logic code should reside. Think of each functionality as a single service, and create a file for it. One file per Service (Functionality)!

### Configuration

#### Server
The server configuration can be found in the *index.js* file located on the root of the Project Directory.

#### Routing
To add the routes that you have created on the *controllers folder*, locate the *router.js* file on the root of the Project Directory. Inside, you can see an example of how it is done, along with the kind of passport strategy to use if you choose to protect certain routes.

#### Secret
Before running the project, make sure to create a *config.js* file in the project directory and copy the following into it.
```javascript
// Hold application secrets and config
module.exports = {
    secret: "testing"
};
```
This is for holding the secret that will be used in creating the JWT. Feel free to change it to anything you want.

### Deployment Instructions
With the **Terminal** pointing to the Project Directory, key in the following command:
    
>   npm run dev
